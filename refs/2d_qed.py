#!/usr/bin/python3
# encoding: utf-8

from numpy.lib import ravel_multi_index
from tqdm import tqdm
import numpy as np
from matplotlib import pyplot as plt
from math import exp, ceil, log, sqrt, pi, cos, sin
import random as rand
import gvar as gv


a = 0.1
Nt = int(5/a)
Nx = int(10/a)
beta = 1.0
alpha = 1.0 / (Nx*Nt*beta)



def plq(U,nx,nt):
    return ( U[0][nx][nt] * U[1][(nx+1)%Nx][nt] * ( U[1][nx][nt] * U[0][nx][(nt+1) % Nt]).conjugate() ).real

def Sg(U):
    P = np.zeros((Nx,Nt))
    for nx in range(0,Nx):
        for nt in range(0,Nt):
            P[nx][nt] = plq(U,nx,nt)
    return np.sum(P) *alpha

def update(U):
    Nacp = 0
    ix = np.array(range(0, Nx))
    it = np.array(range(0, Nt))
    rand.shuffle(ix)
    rand.shuffle(it)
    for i in range(0,Nx) :
        for j in range(0,Nt) :
            nx = ix[i]
            nt = it[j]
            ux = U[0][nx][nt]
            ut = U[1][nx][nt]
            nx_nb = (nx-1+Nx) % Nx
            nt_nb = (nt-1+Nt) % Nt
            dS0 = ( plq(U, nx, nt) + plq(U, nx_nb, nt) + plq(U, nx, nt_nb) ) 
            for mu in range (0,2):
                phase=rand.uniform(-pi,pi)
                U[mu][nx][nt] *= complex(cos(phase),sin(phase))
            dS = ( plq(U,nx,nt) + plq(U, nx_nb, nt) + plq(U,nx,nt_nb) ) - dS0   
    
            if  exp(-dS) > rand.uniform(0,1):
                Nacp +=1
            else : 
                U[0][nx][nt] = ux
                U[1][nx][nt] = ut
    
    acp_rate = Nacp/(Nx*Nt)
    # print("Nacp/(Nx*Nt)", acp_rate)
    
    return acp_rate
            


# init U
U = np.ones((2, Nx, Nt), dtype=complex)

# Warm Up
print("Warming up ... ...")
Nwarm = 1000
for n_w in tqdm(range(0, Nwarm)):
    rate_warm = update(U)
    if n_w % 50 == 0 :
        print(" warm updates ", n_w, "acept_rate: ", rate_warm)

# Generate
Nstp = 20
Ncfg = 100
S = np.zeros(Ncfg)
rate_acp = np.zeros(Ncfg)
for n_cfg in tqdm(range(0, Ncfg)):
    for n_stp in range(0,Nstp):
        rate_tmp = update(U)
    rate_acp[n_cfg] = rate_tmp
    S[n_cfg] = Sg(U)

Savg = S.mean()
Serr = sqrt(S.std())

print("S_plq = ", Savg, Serr)
