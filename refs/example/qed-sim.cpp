#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <math.h>
#include <complex>

using namespace std;

#define cmplx complex<double>
#define vec vector<cmplx>
#define mat vector<vector<cmplx> >
#define tensor vector<vector<vector<cmplx> > >

std::complex<double> I(0.0,1.0);

double a=0.1;
double epsl=1.0;

double plaq(tensor &link, int T, int X) 
{
     int nt = link.size();
	 int nx = (link[0]).size();
     int Xf=(X+1)%nx;
     int Tf=(T==nt-1)?0:(T+1);
        return cmplx(link[T][X][0]*link[T][Xf][1]*conj(link[Tf][X][0])*conj(link[T][X][1])).real();
}

double plaq1x2(tensor &link, int T, int X)
{
     int nt = link.size();
     int nx = (link[0]).size();
    int Xf=(X+1)%nx;
        int Tf=(T==nt-1)?0:(T+1);
        return cmplx(link[T][X][0]*link[T][(X+1)%nx][1]*link[(T+1)%nt][(X+1)%nx][1]
               *conj(link[(T+2)%nt][X][0])*conj(link[(T+1)%nt][X][1])*conj(link[T][X][1])).real();
}

double plaq2x2(tensor &link, int T, int X)
{
     int nt = link.size();
     int nx = (link[0]).size();
    
        return cmplx(link[T][X][0]*link[T][(X+1)%nx][0]*link[T][(X+2)%nx][1]*link[(T+1)%nt][(X+2)%nx][1]
                     *conj(link[(T+2)%nt][(X+1)%nx][0])*conj(link[(T+2)%nt][X][0])*conj(link[(T+1)%nt][X][1])*conj(link[T][X][1])).real();
}

double part_of_S(tensor &link, int T, int X, double g_eff)
{
        int nt = link.size();
	    int nx = (link[0]).size();
        int Xb=(X==0)?(nx-1):(X-1);
        int Tb=(T==0)?(nt-1):(T-1);
	return -(plaq(link, T, X)+plaq(link, Tb, X)+plaq(link, T, Xb))/pow(g_eff,2);
}

//
int sample(tensor &link, tensor &link_tmp, int T, int X, double g_eff)
//Determine whether the update will be accepted
{
    double phase=0;
    for(int miu=0;miu<2;miu++)
    {   
        phase=(rand()*M_PI*2/RAND_MAX)-M_PI;
        link_tmp[T][X][miu]=link[T][X][miu]*cmplx(cos(phase),sin(phase));
    }
	//calculate the change of the action, and calculation the acceptation rate;	
	double dS=part_of_S(link_tmp,T,X,g_eff)-part_of_S(link,T,X,g_eff);

        //deside whether the update can be accepted.
        double judge = rand()*epsl/RAND_MAX;     
        if (judge < exp(-dS))
        {
        	//update the configuration
            for(int miu=0;miu<2;miu++)
	        link[T][X][miu]=link_tmp[T][X][miu];
            return 1;
        }
        else
        {
        	//discard the value of the temporary configuration
            for(int miu=0;miu<2;miu++)
	        link_tmp[T][X][miu]=link[T][X][miu];
        	return 0;
	}
}

double iteration(tensor &link, double g_eff)
 {
	//shuffle the order of the sites on the lattice
	int nt = link.size();
	int nx = (link[0]).size();
	vector<int> ordert(nt);
	vector<int> orderx(nx);
	for(int it=0;it<nt;it++) ordert[it]=it;
	for(int ix=0;ix<nx;ix++) orderx[ix]=ix;
	random_shuffle(ordert.begin(), ordert.end());
	random_shuffle(orderx.begin(), orderx.end());

	//the temporary configuration
	tensor link_tmp(nt,mat(nx,vec(2,0.0)));
	for(int it=0; it < nt; it++)
	for(int ix=0; ix < nx; ix++)
        for(int miu=0;miu<2;miu++)
	    link_tmp[it][ix][miu]=link[it][ix][miu];
	
	//update the configuration
	double acp = 0.;
	for(int it=0; it < nt; it++)
	for(int ix=0; ix < nx; ix++)
	        acp += sample(link, link_tmp, ordert[it], orderx[ix], g_eff);
	orderx.clear();
	ordert.clear();
	
	//return the averaged acceptation rate
	return acp *1.0 / (nt * nx);
 }


void measure(int nt, int nx, double a, double g_0, double ncfg)
{
        double g_eff = g_0;
        epsl=pow(g_eff,4);
    
	tensor link(nt,mat(nx,vec(2,0.0)));
        srand((unsigned)time(NULL));
       
	for(int it=0; it < nt; it++)
	for(int ix=0; ix < nx; ix++)
        for(int miu=0;miu<2;miu++)
        	link[it][ix][miu]=1; //1+0i // cold start
	
  	//Warm up
  	double acp = 1.;
  	int traj_sep=100;
  	for (int u = 0; u < 10*traj_sep; u++)//if the acceptation rate is low, do a longer warm up.
  	{
  		acp = iteration(link, g_eff);
 		if(u>100&&u%10==0)if(acp<0.5) epsl*=0.7;
  		if(u%100==0)
  		{
  			double Loop1x1=0.0,Loop1x2=0.,Loop2x2=0.;
  			for(int it=0;it<nt;it++)
  			for(int ix=0;ix<nx;ix++)
            {    Loop1x1+=cmplx(plaq(link,it,ix)).real();
                 Loop1x2+=cmplx(plaq1x2(link,it,ix)).real();
                Loop2x2+=cmplx(plaq2x2(link,it,ix)).real();
            }
  			printf("epsl=%10.5e, acp=%10.5f, Loop1x1=%10.5f, Loop1x2=%10.5f, Loop2x2=%10.5f\n",epsl, acp,Loop1x1/(nt*nx), Loop1x2/(nt*nx), Loop2x2/(nt*nx));
		}
	}
	int t_max=2,t_size=2,x_max=(int)(nx/3);
	tensor loops(ncfg,mat(t_size,vec(x_max,0.0)));
    vector<double> plasquare(ncfg,0.);
	for (int u = 0; u < ncfg; u++)
	{
		for(int i=0;i<traj_sep;i++)
			iteration(link, g_eff);
		printf(".");fflush(stdout);
        
        
        for(int it=0;it<nt;it++)
        for(int ix=0;ix<nx;ix++)
                plasquare[u]+=cmplx(plaq(link,it,ix)).real()/(nt*nx);
        
	}
	printf("\n");
    printf("Loop1x1=%13.5f\n",plasquare[0]);
    
    double aver=0., err2=0.,err=0.;
    for (int u = 0; u < ncfg; u++)
        aver+=plasquare[u]/ncfg;
    
    for (int u = 0; u < ncfg; u++)
    {
        err2+= (plasquare[u]*plasquare[u]-aver*aver)/ncfg;
    }
    
    printf("%20.5f%10.5f\n",aver,sqrt(err2));
    
  
}

int main(int argc, char * argv[])
{
	a = 0.1;
	if(argc>1)a=atof(argv[1]);
	int nx = 5/a;
	int nt = 10/a;
	double g_0 = 1;
	int ncfg=100;
	if(argc>2)g_0=atof(argv[2]);
	if(argc>3)ncfg=atof(argv[3]);
	measure(nt, nx, a, g_0, ncfg);
	
	return 0;
}
