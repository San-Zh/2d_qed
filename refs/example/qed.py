import numpy as np                                            #程序结果并不唯一，仍然使用了shuffle给格点随机排序而非顺序
import random
from tqdm import tqdm
import matplotlib
import matplotlib.pyplot as plt


randlist = np.random.rand(50,100,23000,3) #定义随机表


    
#file1=open("c0R.txt",'w');
#    for i in range(0, len(m2dat)):
#          file1.write(str(m2dat[i])+  "\t"+str(m2daterr[i])+"\n")
#    file1.close()

def plaq(link, T, X):       #1*1 的loop
    nt = np.size(link,0)
    nx = np.size(link,1)
    return (link[T][X][0]*link[T][(X+1)%nx][1]*link[(T+1)%nt][X][0].conjugate()*link[T][X][1].conjugate()).real

def plaq1(link, T, X):      #1*2的loop
    nt = np.size(link,0)
    nx = np.size(link,1)
    return (link[T][X][0]*link[T][(X+1)%nx][0]*link[T][(X+2)%nx][1]*link[(T+1)%nt][(X+1)%nx][0].conjugate()*link[(T+1)%nt][X][0].conjugate()*link[T][X][1].conjugate()).real

def part_of_S(link, T, X, g_eff):  #作用量
    nt = np.size(link,0)
    nx = np.size(link,1)
    return -(plaq(link,T,X)+plaq(link,(T-1)%nt,X)+plaq(link,T,(X-1)%nx))/np.power(g_eff,2)

def sample(link,T,X,g_eff,epsl,cont):   #判断采纳
    link_tmp = link.copy()
                
    for i in range(2):
        phase = (randlist[X][T][int(cont)][i]*2*np.pi-np.pi)*epsl     #第一处random
#        phase = random.uniform(-np.pi,np.pi)  
        link_tmp[T][X][i] = link[T][X][i]*np.exp(phase*1j)
    ds =  part_of_S(link_tmp, T, X, g_eff)- part_of_S(link, T, X, g_eff)
#    prob2=np.random.rand();
    prob2=randlist[X][T][int(cont)][2]
    if prob2 < np.exp(-ds):        #第二处random
        for i in range(2):
            link[T][X][i] = link_tmp[T][X][i]
        return link,1
    else :
        for i in range(2):
            link_tmp[T][X][i] = link[T][X][i]
        return link,0       

def iteration(link,g_eff,epsl,cont):     #更新路径
    nt = np.size(link,0)
    nx = np.size(link,1)
    ordert = np.zeros(nt)
    orderx = np.zeros(nx)
    for i in range(nt):
        ordert[i] = i
    for i in range(nx):
        orderx[i] = i
#    random.shuffle(orderx)
#    random.shuffle(ordert)
    acp = 0
    for it in range(nt):
        for ix in range(nx):
            link,ac = sample(link,int(ordert[it]),int(orderx[ix]),g_eff,epsl,cont)
            acp = acp + ac
    return link,acp/(nt*nx)

def measure(nt,nx,a,g_0,ncfg):     #主程序
    g_eff = g_0
    epsl =np.power(g_eff,4)
    link = np.ones([nt,nx,2],dtype=complex)
    loop = 0 
    acp = 1
    traj_sep = 100
    for u in tqdm(range(int(30*traj_sep))):
        if u>10 and u%10==0 and acp < 0.5:
            epsl =epsl*0.7
        if u%10 == 0 :
            loop = 0 
            for it in range(nt):
                for ix in range(nx):
                    loop = loop + plaq(link,it,ix).real
            print(epsl,acp,loop/(nt*nx))
        link,acp = iteration(link,g_eff,epsl,u)
    t_max = 2
    t_size = 2
    x_max = int(nx/3)
    plasquare = np.zeros(ncfg)
    for u in tqdm(range(ncfg)):
        for i in range(traj_sep):
            link,a = iteration(link,g_eff,epsl,u+3000)
        for it in range(nt):
            for ix in range(nx):
                plasquare[u] += plaq1(link,it,ix).real/(nt*nx)
    aver = np.mean(plasquare)
    err2 = np.std(plasquare)
    return plasquare

a = 0.5
epsl = 1.
nx = int(5/a)
nt = int(10/a)
g_0 = 1
ncfg = 200
p = measure(nt,nx,a,g_0,ncfg)

p.mean()
