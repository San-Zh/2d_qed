#!/usr/bin/python3
# encoding: utf-8
"""
1+1D gauge theory on lattice
"""

import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm
import os
import pickle


class QED2D(object):
    def __init__(self, Nx, Nt, delta=1.0, beta=1.0):
        self.Nx = Nx
        self.Nt = Nt
        self.delta = delta
        self.beta = beta
        self.alpha = 1.0 / (Nx * Nt * beta)

    def get_conf(self):
        return np.ones((2, self.Nx, self.Nt), dtype=complex)

    @staticmethod
    def save_conf(U, file_out):
        f = open(file_out, "wb")
        pickle.dump(U, f)
        f.close()

    @staticmethod
    def load_conf(file_in):
        f = open(file_in, "rb")
        U = pickle.load(f)
        f.close()
        return U

    @staticmethod
    def plt_conf(U, file_out):
        plt.figure(figsize=(12, 12))
        d, Nx, Nt = U.shape
        assert d == 2
        xreal = np.real(U[0, :, :])
        ximag = np.imag(U[0, :, :])

        tv = np.angle(U[1, :, :])
        xr = np.arange(Nx)
        tr = np.arange(Nt)
        T, X = np.meshgrid(tr, xr)

        plt.quiver(
            T, X, xreal, ximag, units="width", scale=80, width=0.001, headwidth=3
        )
        plt.scatter(T, X, c=tv, s=2, cmap="rainbow")
        plt.xlabel("T")
        plt.ylabel("X")
        plt.grid(True)
        plt.savefig(file_out)
        plt.close('all')

    def plt_conf_plq(self, U, file_out):
        plt.figure(figsize=(12, 12))
        P = np.zeros((self.Nx, self.Nt))
        for nx in range(self.Nx):
            for nt in range(self.Nt):
                P[nx, nt] = self.plaq(U, nx, nt)
        plt.matshow(P)
        plt.xlabel("T")
        plt.ylabel("X")
        plt.grid(True)
        plt.savefig(file_out)
        plt.close('all')

    def plaq(self, U, nx, nt):
        # on plaquette loop
        return 1.0 - (
            U[0, nx, nt]
            * U[1, (nx + 1) % self.Nx, nt]
            * (U[1, nx, nt] * U[0, nx, (nt + 1) % self.Nt]).conjugate()
        ).real

    def action_of_gauge(self, U):
        P = np.zeros((self.Nx, self.Nt))
        for nx in range(self.Nx):
            for nt in range(self.Nt):
                P[nx, nt] = self.plaq(U, nx, nt)
        return np.sum(P) * self.alpha

    def update(self, U):
        Nacp = 0
        utmp = np.zeros(2, dtype=complex)
        ix = np.arange(self.Nx)
        it = np.arange(self.Nt)
        np.random.shuffle(ix)
        np.random.shuffle(it)
        for i in range(self.Nx):
            for j in range(self.Nt):
                nx = ix[i]
                nt = it[j]
                utmp[:] = U[:, nx, nt]
                nx_nb = (nx - 1 + self.Nx) % self.Nx
                nt_nb = (nt - 1 + self.Nt) % self.Nt
                dS0 = (
                    self.plaq(U, nx, nt)
                    + self.plaq(U, nx_nb, nt)
                    + self.plaq(U, nx, nt_nb)
                )
                # for mu in range(2):
                # phase = self.delta * np.random.uniform(-np.pi, np.pi)
                # U[mu, nx, nt] *= complex(np.cos(phase), np.sin(phase))
                phase = self.delta * \
                    np.random.uniform(-np.pi, np.pi, size=(2,))
                U[:, nx, nt] *= np.exp(1j * phase)
                dS = (
                    self.plaq(U, nx, nt)
                    + self.plaq(U, nx_nb, nt)
                    + self.plaq(U, nx, nt_nb)
                ) - dS0

                if np.exp(-dS) > np.random.uniform(0, 1):
                    Nacp += 1
                else:
                    U[:, nx, nt] = utmp[:]

        acp_rate = Nacp / (self.Nx * self.Nt)
        # print("Nacp/(Nx*Nt)", acp_rate)
        return acp_rate

    def warmup(self, U, Nwarm=1000):
        for n_w in tqdm(range(Nwarm)):
            rate_warm = self.update(U)
            if n_w > 0 and (n_w % 50 == 0):
                print("  warmup at %d, acceptance rate: %g" % (n_w, rate_warm))

    def generate(self, U, Ncfg=100, Nstep=20, conf_saved=True):
        S = np.zeros(Ncfg)
        rate_acc = np.zeros(Ncfg)
        if conf_saved:
            if not os.path.isdir("fig"):
                os.mkdir("fig", 0o755)
            if not os.path.isdir("dat"):
                os.mkdir("dat", 0o755)
        for n_cfg in tqdm(range(Ncfg)):
            for n_stp in range(Nstep):
                rate_tmp = self.update(U)
            rate_acc[n_cfg] = rate_tmp
            S[n_cfg] = self.action_of_gauge(U)
            if conf_saved:
                self.save_conf(U, os.path.join("dat", "conf%d.dat" % (n_cfg)))
                self.plt_conf(U, os.path.join("fig", "conf%d.png" % (n_cfg)))
                self.plt_conf_plq(U, os.path.join(
                    "fig", "plq_conf%d.png" % (n_cfg)))
        return S, rate_acc


def plt_action(S, rate_acc):
    plt.figure()
    plt.subplot(2, 1, 1)
    plt.plot(range(len(S)), S, "r.")
    plt.ylabel("action of gauge")
    plt.subplot(2, 1, 2)
    plt.plot(range(len(rate_acc)), rate_acc, "b.")
    plt.ylabel("acceptance rate")
    plt.xlabel("configuration")
    plt.savefig("gauge_action.png")
    plt.close()


def main():
    a = 0.1
    Nt = int(4 / a)
    Nx = int(4 / a)
    delta = 0.1
    qed = QED2D(Nx, Nt, delta, beta=1.0)
    U = qed.get_conf()
    Nwarm = 0
    print("Warming up with %d cfgs ... ..." % Nwarm)
    qed.warmup(U, Nwarm)
    qed.save_conf(U, os.path.join("dat", "conf_warm.dat"))

    Ncfg = 150
    Nstep = 1
    print("Generating %d cfgs every %d steps ..." % (Ncfg, Nstep))
    S, rate_acc = qed.generate(U, Ncfg, Nstep, conf_saved=True)

    print("Action of gauge:", S.mean(), np.sqrt(S.std()))
    plt_action(S, rate_acc)


if __name__ == "__main__":
    from sys import argv

    main()
