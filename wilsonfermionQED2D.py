#!/usr/bin/python3
# encoding: utf-8

"""
class WlisonFermion:
    定义了 Wilson 费米子 的 （Dslash * phi） and （Dslash^\dagger phi） 操作,
    其中的 Dslash 是Wilson费米子相关
Dslash 形式如下：
D = (2+m) \delta_{x,t;x',t'} - (1/2) [
    (1-\gamma_0) U_{0} (x,t) \delta_{x+1,t; x',t'}  + (1+\gamma_0) U^\dagger_{0}(x-1,t) \delta_{x-1,t; x',t'}
    + (1-\gamma_1) U_{1} (x,t) \delta_{x+1,t; x',t'}  + (1+\gamma_1) U^\dagger_{1}(x,t-1) \delta_{x,t-1; x',t'}
    ] 

坐标维度： 0，1--> direction: x,t
\gamma_0 = [[ 0,-i ],       
            [ i, 0 ]]    
\gamma_1 = [[ 0, 1 ],       
            [ 1, 0 ]]
\gamma_5 = [[-i, 0 ],
            [ 0, i ]]

"""

import numpy as np
from tqdm import tqdm
import pickle


class WilsonFermion(object):
    def __init__(self, X,  mass=0.1):
        self.DRC = 2
        self.CLR = 1
        self.DIM = 2
        self.X = X   # X[0] X[1]= Lx, Lt
        self.mass = mass
        self.U = np.zeros((self.DIM, self.X[0], self.X[1]), dtype=complex)
        self.Gammaidx = np.array([[1, 0],
                                 [1, 0],
                                 [0, 1]])
        self.Gamma = np.array([[-1j, 1j],
                               [1,  1],
                               [1, 1]], dtype=complex)

    def create_fermion(self):
        return np.zeros((self.DRC, self.X[0], self.X[1]), dtype=complex)

    def create_pointsrc(self, dr, x):
        src = self.create_fermion()
        src[dr, x[0], x[1]] = 1.0
        return src

    def create_prop(self):
        prop = np.zeros((self.DRC, self.DRC,
                        self.X[0], self.X[1]), dtype=complex)
        return prop

    def prop_dagger(self, prop):
        return np.transpose(np.conj(prop), (1, 0, 2, 3))

    def gamma_mul_phi(self, dim, dc, phi, dagger):
        if dagger:
            return self.Gamma[dim, dc] * phi[self.Gammaidx[dim, dc]]
        else:
            return self.Gamma[dim, dc] * np.conj(phi[self.Gammaidx[dim, dc]])

    """ 组态：从文件读取组态数据；或 产生 “unit” or “random” 组态"""

    def load_conf(self, file_in):
        f = open(file_in, "rb")
        self.U = pickle.load(f)
        f.close()
        print(f"[GAUGE] load gauge configuration from file: {file_in}")

    def get_conf(self, type="unit"):
        if type == "unit":
            self.U = np.ones((2, self.X[0], self.X[1]), dtype=complex)
        if type == "random":
            self.U = np.exp(1j * np.random.uniform(-np.pi,
                            np.pi, size=(2, self.X[0], self.X[1])))
        print(f"[GAUGE] {type} gauge configuration using")

    """ Dlsash 相关操作,"""

    def shift(self, phi, dim, direction):
        # dim = 0/t --> x/t-dim; direction = +1/-1
        ix = (np.arange(self.X[0]) + direction) % self.X[0]
        if dim == 0:
            return phi[:, ix, :]
        elif dim == 1:
            return phi[:, :, ix]

    def gamma_mul_phi(self, phi, dim):
        res = self.create_fermion()
        for drc in range(self.DRC):
            res[drc] = self.Gamma[dim, drc] * phi[self.Gammaidx[dim, drc]]
        return res

    def project(self, phi, dim, direction):
        # @direction： +/-, 分别对应 (1 -/+ \gamma_\mu) phi （注意刚好相反）
        return phi - direction * self.gamma_mul_phi(phi, dim)

    """ Dslash"""
    """ 
    关于 U * phi 这里三种写法，以方法三为基准：
    方法一约有 15% 的效率下降, 原因：推测是多了一次（共3次）到 chitmp 的写操作
    方法二约有 20% 的效率提升 （numpy broadcast？）
    # 方法一
    # chitmp = self.shift(phi, dim, direction)
    # chitmp[0] *= self.U[dim]
    # chitmp[1] *= self.U[dim]
    # 方法二
    # chitmp = self.U[dim] * self.shift(phi, dim, direction)
    # 方法三
    # chitmp[0] = self.U[dim] * self.shift(phi, dim, direction)[0]
    # chitmp[1] = self.U[dim] * self.shift(phi, dim, direction)[1]
    """

    def D(self, phi, dagger=False):
        # dag = -/+  --> dagger = False/True
        dag = dagger*2 - 1
        res = self.create_fermion()
        # dim = 0, direction = +1
        tmp = self.U[0] * self.shift(phi, 0, +1)
        res = self.project(tmp, 0, +1*dag)
        # dim = 0, direction = -1
        tmp = self.shift(np.conj(self.U[0]) * phi, 0, -1)
        res += self.project(tmp, 0, -1*dag)
        # dim = 1, direction = +1
        tmp = self.U[1] * self.shift(phi, 1, +1)
        res += self.project(tmp, 1, +1*dag)
        # dim = 1, direction = -1
        tmp = self.shift(np.conj(self.U[1]) * phi, 1, -1)
        res += self.project(tmp, 1, -1*dag)

        # direction = +1  # forward diff
        # for dim in range(self.DIM):
        #     tmp = self.U[dim]*self.shift(phi, dim, direction)
        #     res += self.project(tmp, dim, direction*dag)
        # direction = -1  # backward diff
        # for  dim in range(self.DIM):
        #     tmp = self.shift(np.conj(self.U[dim])*phi, dim, direction)
        #     res += self.project(tmp, dim, direction*dag)

        # diag term
        res = (2.0 + self.mass) * phi - 0.5*res
        return res

    def DdD(self, phi):
        return self.D(self.D(phi), True)

    @staticmethod
    def norm(x, y):
        return np.real(np.vdot(x, y))


def main():
    X = [40, 40]  # QED
    mass = 0.1
    A = WilsonFermion(X, mass)
    A.get_conf("unit")
    # A.get_conf("random")
    # A.load_conf("./dat/conf121.dat")

    # make source
    x_src = [0, 0]
    src = A.create_pointsrc(0, x_src)
    # src = np.exp(1j * np.random.uniform(-np.pi, np.pi, size=(2, X[0], X[1])))

    # check
    q = src
    Dq = A.D(q)
    Ddq = A.D(q, True)
    DdDq = A.DdD(q)
    qDq = np.vdot(q, Dq)
    DqDq = np.vdot(Dq, Dq)
    qDdDq = np.vdot(q, DdDq)
    # print("  sum(D * q) =", np.sum(Dq))
    # print("  sum(D^\dagger * D * q ) =", np.sum(DdDq))
    # print("  sum(D^\dagger * q ) =", np.sum(Ddq))
    print("  qDq = q^\dagger * D * q = ", qDq)
    print("  qDdDq = q^\dagger * D^\dagger * D * q = ", qDdDq)
    print("  DqDq = (D*q)^\dagger * (D*q) =", np.vdot(Dq, Dq))
    # print("  DdqxDdQ(D^\dagger*q)^\dagger * (D^\dagger*q) =", np.vdot(Ddq, Ddq))
    # print(np.conj(A.U)*A.U)


if __name__ == "__main__":
    from sys import argv
    main()
