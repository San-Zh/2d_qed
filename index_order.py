#!/usr/bin/python3
# encoding: utf-8

from operator import index
from re import I
import numpy as np

Nx = 5
Ny = 4
Nz = 4
Nt = 2
XT = [Nx, Ny, Nz, Nt]
XTh = [Nx, Ny, Nz, Nt]
Xh = int(Nx/2)
V = Nx*Ny*Nz*Nt
Vh = int(V/2)


def idx(x, y, z, t):
    return (t+Nt) % Nt * Nz * Ny * Nx + (z+Nz) % Nz * Ny * Nx + (y+Ny) % Ny * Nx + (x+Nx) % Nx


def eo(x, y, z, t):
    return (x+y+z+t) % 2


def idxeo(x, y, z, t):
    return int(idx(x, y, z, t)/2) + eo(x, y, z, t) * Vh


def FV():
    Fv = np.zeros(V)
    ni = 0
    for nt in range(Nt):
        LLL = nt * 1000
        for nz in range(Nz):
            LL = nz*100
            for ny in range(Ny):
                L = ny*10
                for nx in range(Nx):
                    Fv[ni] = LLL + LL + L + nx
                    ni = ni + 1
    return Fv


def FVeo():
    Feo = FV()
    Ftmp = FV()
    for nt in range(Nt):
        for nz in range(Nz):
            for ny in range(Ny):
                for nx in range(Nx):
                    Feo[idxeo(nx, ny, nz, nt)] = Ftmp[idx(nx, ny, nz, nt)]
    return Feo


def FVeoF():
    Feo = FV()
    Ftmp = FV()
    for nt in range(Nt):
        for nz in range(Nz):
            for ny in range(Ny):
                for nx in range(Nx):
                    Feo[idxeo(nx, ny, nz, nt)] = Ftmp[idx(nx+1, ny, nz, nt)]
    return Feo


def prd_Xitom(XT, axi):
    Res = 1
    for it in range(axi):
        Res = Res*XT[it]
    return Res


def BoundaryEO(F):
    Fp = FVeo()
    # SLh = Ny*Xh
    # SLsh = Xh
    # indx = np.arange(0, SLh)
    # indxpr = (indx + SLsh) % SLh
    # for ni in range(int(V/SLh)):
    #     Fp[ni*SLh + indx] = F[ni*SLh + indxpr]
    indx = np.arange(0, Xh)
    for nt in range(Nt):
        for nz in range(Nz):
            for ny in range(Ny):
                itzy = nt * Nz * Ny + nz * Ny + ny
                indxpro = (indx + (nt+nz+ny) % 2) % Xh
                indxpre = (indx + (nt+nz+ny+1) % 2) % Xh
                Fp[itzy*Xh + indx] = F[itzy*Xh + indxpre]
                Fp[Vh + itzy*Xh + indx] = F[Vh + itzy*Xh + indxpro]
    return Fp


def main():
    Fidx = FV()
    Feo = FVeo()
    Feof = FVeoF()
    ni = 0
    Xh = int(Nx/2)
    FeoP = BoundaryEO(Feo)
    print("(t, z, y, x)  Feo         Feof         FeoP      EO   Feof-FeoP  ")
    for nt in range(Nt):
        print("_______________________________________________________________________________________")
        for nz in range(Nz):
            print("_________________________________________________________________________________")
            for ny in range(Ny):
                # print("_____________________________________________________________________")
                niy = ni
                for nx in range(Nx):
                    # print("%5d"%Fidx[ni])
                    # ni = ni+1
                    if(eo(nx, ny, nz, nt) == 0):
                        print("(%d %d %d %d) |%4d%5d| |%4d%5d| |%4d%5d|  %5d  %5d" %
                              (nt, nz, ny, nx, Feo[ni], Feo[ni+Vh], Feof[ni], Feof[ni+Vh], FeoP[ni], FeoP[ni+Vh],  (ny + nz+nt) % 2, Feof[ni] - FeoP[ni+Vh]))
                        ni = ni+1
                    # if(nx==int(Nx/2)):
                    #     print("  ----------")

    ni = np.arange(Vh)
    Fde = Feof[ni+Vh] - FeoP[ni]
    Fdo = Feof[ni] - FeoP[ni+Vh]
    nrm2e = np.dot(Fde, Fde)
    nrm2o = np.dot(Fdo, Fdo)
    print(nrm2e, nrm2o)

    # print("%4d%4d  %4d%4d  %4d%4d  %4d%4d" % (Fidx[ni], Fidx[Vh+ni], Feo[ni],
    #       Feo[Vh+ni],  Feof[ni],  Feof[Vh+ni]))

    # for ni in range(Vh):
    #     if(ni % int(Nx/2) == 0):
    #         if(ni % Nx == 0):
    #             print("-----------------------------------------")
    #         else:
    #             print("  ------    ------    ------    ------")
    #     print("%4d%4d  %4d%4d  %4d%4d  %4d%4d" % (
    #         INDEX[ni], INDEX[Vh+ni], INDEXF[ni], INDEXF[Vh+ni], Feo[ni], Feo[Vh+ni], Feof[ni], Feof[Vh+ni]))
    # print("%4d%4d%4d" % (INDEX[ni], Feo[ni], Feof[ni]))

    # for ni in range(int(Vh/Nx)):
    #     for nx in range(Nx):
    #         print(Feo[ni*Nx + nx]-Feof[ni*Nx + nx])
    #     print("--------------")

    # EO = 1
    # for nt in range(Nt):
    #     for nz in range(Nz):
    #         for ny in range(Ny):
    #             EOflg = eo(EO, ny, nz, nt)
    #             if(EOflg % 2 == EO):
    #                 Feof[EO*Vh+Nx]
    #                 for nx in range(Nx):
    #                     Feof[eoidxf] = INDEX[index]

    # for nt in range(Nt):
    #     for nz in range(Nz):
    #         for ny in range(Ny):
    #             print("---",nt,nz,ny,"---", nt+nz+ny)
    #             for nx in range(Nx):
    #                 index = idx(nx, ny, nz, nt)
    #                 print(Feo[index]-Feof[index])

    # print(Feo[Vh:V])
    # print(Feof[0:Vh])


if __name__ == "__main__":
    from sys import argv
    main()
